﻿using System;
using System.Collections.Generic;
using System.Text;
using ExcelLibrary.BinaryFileFormat;

namespace ExcelLibrary.SpreadSheet
{
    public enum Alignment: byte
    {
        Left = 0,
        Centered,
        Right,
        Block
    }

    public class CellStyle
    {
        //public Color BackColor = Color.White;
        public Alignment Alignment = Alignment.Left;
        public RichTextFormat RichTextFormat;
    }
}
